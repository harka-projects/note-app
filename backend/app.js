import cors from 'cors'
import sqlite3 from 'sqlite3'
import express from 'express'
import bcrypt from 'bcrypt'

const saltRounds = 12
const app = express()
const db = new sqlite3.Database(
  '/home/daniel/code/note-app/database/note_app.db'
)

app.use(cors({ origin: 'https://note-app.harka.com' }))
app.use(express.json())

app.get('/note-app/', (req, res) => res.redirect('https://note-app.harka.com'))

app.get('/note-app/stats', (req, res) => {
  const query = `SELECT
    (SELECT COUNT(*) FROM users) AS userCount,
    (SELECT COUNT(*) FROM notes) AS noteCount;`

  db.get(query, (err, row) => {
    if (err) throw err

    const stats = {
      users: row.userCount,
      notes: row.noteCount,
    }

    res.status(200).json(stats)
  })
})

app.post('/note-app/user-notes', (req, res) => {
  const userId = req.body.userId
  const query = `SELECT * FROM notes WHERE user_id = ? ORDER BY note_id DESC;`

  db.all(query, [userId], (err, rows) => {
    if (err) throw err
    res.status(200).json(rows)
  })
})

app.post('/note-app/new-note', (req, res) => {
  userId = req.body.userId
  title = req.body.title
  content = req.body.content
  pub = req.body.isPublic
  shared = 0

  const query = `INSERT INTO notes
  (note_id, user_id, title, content, public, shared)
  VALUES (?, ?, ?, ?, ?, ?);`

  db.run(query, [null, userId, title, content, pub, shared], err => {
    if (err) throw err
  })

  return res.send(req.body).status(200)
})

app.put('/note-app/update-note', (req, res) => {
  noteId = req.body.note_id
  userId = req.body.user_id
  title = req.body.title
  content = req.body.content
  isPublic = req.body.public

  const query = `
  UPDATE notes
  SET note_id = ?,
      user_id = ?,
      title = ?,
      content = ?,
      public = ?
  WHERE note_id = ?
  `

  db.run(query, [noteId, userId, title, content, isPublic, noteId], err => {
    if (err) throw err
    return res.status(201).json('Note saved successfully!')
  })
})

app.put('/note-app/delete-note', (req, res) => {
  noteId = req.body.note_id

  const query = `
  DELETE FROM notes
  WHERE note_id = ?
  `

  db.run(query, [noteId], err => {
    if (err) throw err
    return res.status(201).json('Note deleted successfully!')
  })
})

app.get('/note-app/public-notes', (req, res) => {
  const query = `
  SELECT users.last_name, users.first_name, notes.title, notes.content
  FROM users
  INNER JOIN notes
  ON users.id = notes.user_id
  WHERE users.id = notes.user_id AND notes.public = 1
  ORDER BY note_id DESC
  ;`

  db.all(query, (err, rows) => {
    if (err) throw err
    res.status(200).json(rows)
  })
})

app.post('/note-app/register', async (req, res) => {
  const firstName = req.body.firstName
  const lastName = req.body.lastName
  const email = req.body.email
  const password = req.body.password
  const hash = await bcrypt.hash(password, saltRounds)
  const query = `
  INSERT INTO users (id, email, password, first_name, last_name)
  VALUES (null, ?, ?, ?, ?)`

  db.run(query, [email, hash, firstName, lastName], function (err) {
    if (err) {
      if (err.code === 'SQLITE_CONSTRAINT') {
        return res.status(200).send({
          message: 'This email address is already registered!',
        })
      } else throw err
    }

    return res.status(200).send({
      success: 'Success! You can now log in.',
    })
  })
})

app.post('/note-app/login', async (req, res) => {
  try {
    const email = req.body.email
    const password = req.body.password
    const query = `SELECT * FROM users WHERE email = ?`

    db.get(query, [email], async (err, row) => {
      if (err) throw err
      this.user = row
      if (this.user) {
        const validPassword = await bcrypt.compare(password, this.user.password)
        if (validPassword) {
          res.status(200).send({
            message: 'Valid email and password!',
            userId: this.user.id,
            email: this.user.email,
            firstName: this.user.first_name,
            lastName: this.user.last_name,
            isLoggedIn: true,
          })
        } else {
          res.status(200).send({
            message:
              'Invalid password! For customer support, an email can be sent to note-app@harka.com from this user email for a temporary recovery password.',
          })
        }
      } else {
        res.status(200).send({
          message: 'Email not registered!',
        })
      }
    })
  } catch (e) {
    res.status(200).send({
      message: 'Something broke!',
    })
  }
})

app.put('/note-app/settings', async (req, res) => {
  try {
    const userId = req.body.userId
    const email = req.body.email
    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const currentPassword = req.body.currentPassword
    const newPassword = req.body.newPassword
    const newPasswordHash = await bcrypt.hash(newPassword, saltRounds)
    const query = `SELECT * FROM users WHERE id = ?`

    db.get(query, [userId], async (err, row) => {
      if (err) throw err
      this.user = row
      if (this.user) {
        const validPassword = await bcrypt.compare(
          currentPassword,
          this.user.password
        )
        if (validPassword) {
          let query

          if (newPassword === '') {
            query = `
          UPDATE users
          SET id = ?,
              email = ?,
              password = ?,
              first_name = ?,
              last_name = ?
          WHERE id = ?`
            db.run(
              query,
              [userId, email, this.user.password, firstName, lastName, userId],
              err => {
                if (err) throw err
                res.status(200).send({
                  success: 'Settings saved successfully!',
                })
              }
            )
          } else {
            query = `
            UPDATE users
            SET id = ?,
                email = ?,
                password = ?,
                first_name = ?,
                last_name = ?
            WHERE id = ?`
            db.run(
              query,
              [userId, email, newPasswordHash, firstName, lastName, userId],
              err => {
                if (err) throw err
                res.status(200).send({
                  success: 'Settings saved successfully!',
                })
              }
            )
          }
        } else {
          res.status(200).send({
            message: 'Invalid current password!',
          })
        }
      } else {
        res.status(200).send({
          message: 'User not found!',
        })
      }
    })
  } catch (e) {
    res.status(200).send({
      message: 'Something broke!',
    })
  }
})

const server = app.listen(3013)
// setTimeout(() => server.close(), 2000);
