import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UrlService {
  serverUrl = 'https://server.harka.com/note-app';

  constructor() {}
}
