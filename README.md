# Note App

Simple note taking for anyone.

## Resources

### [Website](https://note-app.harka.com)

Mostly in English with some Hungarian content.

### [Documentation](https://gitlab.com/harka-portfolio/note-app)

Mostly in English with some Hungarian content.

### [Presentation](https://note-app.harka.com/presentation/)

Mostly in Hungarian with some English content.

### [Database files](https://gitlab.com/harka-portfolio/note-app/-/tree/main/database)

Only in English.

### [Frontend files](https://gitlab.com/harka-portfolio/note-app/-/tree/main/frontend)

Only in English.

### [Backend files](https://gitlab.com/harka-portfolio/note-app/-/tree/main/backend)

Only in English.

### [Desktop files](https://gitlab.com/harka-portfolio/note-app/-/tree/main/desktop)

Only in English.

### [Project files](https://gitlab.com/harka-portfolio/note-app/-/tree/main/docs/project)

Both English and Hungarian content.

### [Lecture notes](https://gitlab.com/harka-portfolio/note-app/-/blob/main/docs/project/notes.md)

Mostly in Hungarian with some English content.

### [Introduction video](https://youtu.be/cMt9knuCaPk)

Mostly in Hungarian with some English content.

## Creators

Developed by **[bjozsef02](https://github.com/bjozsef02)** and **[MrDanielHarka](https://gitlab.com/users/MrDanielHarka/groups)**.

[Top ↑](#note-app)
